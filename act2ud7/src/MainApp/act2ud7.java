 /**
 * 
 */
package MainApp;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Scanner;

/**
 * @author Elisabet Sabat�
 *
 */
public class act2ud7 {

	/**
	 * @param args
	 */
	
	
	public static void main(String[] args) {
		Scanner teclat = new Scanner(System.in);
		Hashtable<String, Double> productesCompra = new Hashtable<String, Double>();
		Hashtable<String, Double> preuProductes = new Hashtable<String, Double>();
		Hashtable<String, Double> tipoIVA = new Hashtable<String, Double>();
		boolean AfegirMesProductes = true;
		double preu, preuIVA;
		int quantitatProductes;
		String producte, resposta = "";
		while(AfegirMesProductes) {
				System.out.println("Introdueix el producte: ");
				producte = teclat.next();
				System.out.println("Introdueix el preu:");
				preu = teclat.nextDouble();
				preuIVA = producteAmbIVA(preu, producte, tipoIVA);
				preuProductes = afegirHash(preuProductes, preuIVA, producte);
				System.out.println("Introdueix la quantitat: ");
				quantitatProductes = teclat.nextInt();
				productesCompra = afegirHash(productesCompra, quantitatProductes, producte);
				
				boolean respValida = false;
				while(!respValida) {
				System.out.println("Vol afegir un altre producte? ");
				resposta = teclat.next();
				
				if(resposta.equalsIgnoreCase("si")||resposta.equalsIgnoreCase("no")) 
					respValida = true;					
				else 
					System.out.println("La seva resposta no est� entre les opcions. Resp�n si/no");
				
				}
			
				if(resposta.equalsIgnoreCase("no"))
					AfegirMesProductes = false;
				
				
			}
		
		visualitzaCompra(productesCompra, preuProductes, tipoIVA);
		System.out.println("Preu total de la compra " + calcularTotal(preuProductes, productesCompra));	
	}
	
	public static double calcularTotal(Hashtable<String, Double> preuProductes, Hashtable<String, Double> productesCompra) {
		Enumeration<String> producte = preuProductes.keys();
		Enumeration<Double> preu = preuProductes.elements();
		Enumeration<Double> quantitat = productesCompra.elements();
		String clave;
		double total = 0, preuElement;
		while(producte.hasMoreElements()) {
			clave = producte.nextElement();
			preuElement = preuProductes.get(clave)*productesCompra.get(clave);
			total = total + preuElement;
		}
		return total;
	}
	
	public static Hashtable<String, Double> afegirHash(Hashtable<String, Double> hastable, double numero, String producte){
		hastable.put(producte, numero);
		return hastable;
	}
	
	
	public static double producteAmbIVA(double preu, String producte, Hashtable<String, Double> tipoIVA) {	
		double IVA = tipusIVA(producte);
		tipoIVA = afegirHash(tipoIVA, IVA, producte);
		return preu + preu*tipusIVA(producte);
	}
		
	
	public static double tipusIVA(String producte) {
		String[] alimentsPrimeraNecessitat = {"Pa","Cereals","Verdura","Formatge","Ous","Llet","Fruita"};
		int contador = 0; 
		double IVA = 0.21;
		while(contador<alimentsPrimeraNecessitat.length) {
			if(producte.equalsIgnoreCase(alimentsPrimeraNecessitat[contador]))
				IVA = 0.04;
			
			contador++;
		}
		return IVA;	
	}
	
	public static void visualitzaCompra (Hashtable<String, Double> productesCompra, Hashtable<String, Double> preuProductes, Hashtable<String, Double> tipoIVA) {
		Enumeration<String> producte = productesCompra.keys();
		Enumeration<Double> quantitat = productesCompra.elements();
		Enumeration<Double> IVA = tipoIVA.elements();
		Enumeration<Double> preu = preuProductes.elements();
		
		while(producte.hasMoreElements()) {
			System.out.println("-----------\nProducte: " + producte.nextElement() + " \nquantitat: " + quantitat.nextElement() + "\nIVA: " +IVA.nextElement() +"\nPreu: " + preu.nextElement());
		}
	}
	



	
}
