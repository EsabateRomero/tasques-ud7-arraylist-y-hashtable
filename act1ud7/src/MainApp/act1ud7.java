/**
 * 
 */
package MainApp;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import java.util.Scanner;

/**
 * @author Elisabet Sabat�
 *
 */
public class act1ud7 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner teclat = new Scanner(System.in);
		Hashtable<String, Integer> alumnesNotes = new Hashtable<String, Integer>();
		boolean AfegirMesAlumnes = true;
		String alumne;
		while(AfegirMesAlumnes) {
				System.out.println("Introdueix l'alumne: ");
				alumne = teclat.next();
				ArrayList<Integer> notes = afegirNotes(teclat);
				VisualitzaNotes(notes, alumne);
				int mitjanaAlumne = CalcularMitjana(notes);
				alumnesNotes = AfegirAlumne(alumnesNotes, mitjanaAlumne, alumne);
				
				boolean respostaValida = false;
				while(!respostaValida) { 
				System.out.println("Voleu afegir un altre alumne? SI/NO");
				String resposta = teclat.next();
				if(resposta.equalsIgnoreCase("no")||resposta.equalsIgnoreCase("si")) {
					respostaValida = true;
				} 
				if(resposta.equalsIgnoreCase("no"))
					AfegirMesAlumnes = false;
				}
		}
		visualitzaAlumnes(alumnesNotes);
	}
	
	public static void visualitzaAlumnes (Hashtable<String, Integer> alumnes) {
		Enumeration<String> nomsAlumnes = alumnes.keys();
		Enumeration<Integer> notesAlumnes = alumnes.elements();
		while(nomsAlumnes.hasMoreElements()) {
			System.out.println("-----------\nNom del alumne: " + nomsAlumnes.nextElement() + " \nnota mitjana: " + notesAlumnes.nextElement());
		}
	}
	
	public static int CalcularMitjana(ArrayList<Integer> notes) {
		int mitjana = 0;
		for (int i = 0; i < notes.size(); i++) {
		     mitjana = mitjana + notes.get(i);
		}
		mitjana = mitjana/notes.size();
		return mitjana;
	}
	
	public static Hashtable<String, Integer> AfegirAlumne(Hashtable<String, Integer> alumnesNotes, int mitjana, String alumne){
		alumnesNotes.put(alumne, mitjana);
		return alumnesNotes;
	}
	
	public static ArrayList<Integer> afegirNotes(Scanner teclat) {
		ArrayList<Integer> notes = new ArrayList<Integer>();
		System.out.println("Ara podeu escriure les notes d'aquest alumne");
		System.out.println("Per a deixar de posar notes escrigui -1");
		while(true) {
			System.out.println("Introdueix la nota:");
			int nota = teclat.nextInt();
			if(nota==-1)
				break;
			if(nota>=0&&nota<=10)
				notes.add(nota);
			else
				System.out.println("Nota no v�lida");
		}
		return notes;
	}
	
	public static void VisualitzaNotes(ArrayList<Integer> notes, String alumne) {
		System.out.println("Heu afegit les notes: ");
		for (int i = 0; i < notes.size(); i++) {
		     System.out.println("---> " + notes.get(i));
		}
		System.out.println("Per l'alumne "+ alumne);
	}
	
	
}
