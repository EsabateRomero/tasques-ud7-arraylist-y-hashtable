 /**
 * 
 */
package MainApp;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Scanner;

import javax.sound.midi.Soundbank;

/**
 * @author Elisabet Sabat�
 *
 */
public class act3ud7 {

	/**
	 * @param args
	 */
	
	
	public static void main(String[] args) {
		Scanner teclat = new Scanner(System.in);
		System.out.println("BENVOLGUT!");
		Hashtable<String, Double> productesCompra = new Hashtable<String, Double>();
		Hashtable<String, Double> preuProductes = new Hashtable<String, Double>();
		productesCompra = productesCompra(productesCompra);
		preuProductes = preuProductes(preuProductes);
		boolean sortir = false;
		while(!sortir) {
		switch (opcio(teclat)) {
		case 1:
			afegirNouProducte(teclat, productesCompra, preuProductes);
			break;

		case 2:
			
			VisualitzarProductes(productesCompra, preuProductes);
			break;
			
		case 3:
			buscarProducte(teclat, productesCompra,preuProductes);
			break;
			
		case 4:
			System.out.println("Heu sortit.");
			sortir = true;
			break;
		}
		}
	}
	
	public static void buscarProducte(Scanner teclat, Hashtable<String, Double> productesCompra, Hashtable<String, Double> preuProductes) {
		Enumeration<String> producte = productesCompra.keys();
		Enumeration<Double> quantitat = productesCompra.elements();
		Enumeration<Double> preu = preuProductes.elements();
		boolean trobat = false;
		System.out.println("Quin producte voleu cercar?");
		String producteCercar = teclat.next();
		while(producte.hasMoreElements()) {
			if(producteCercar.equals(producte.nextElement())) {
				
			System.out.println("-----------\nHeu buscat informaci� de: " + producteCercar + " \nquantitat: " +productesCompra.get(producteCercar) +"\nPreu: " + preuProductes.get(producteCercar));
			trobat = true;
			}
			
		}
		
		if(!trobat) System.out.println("Ho sentim, no hem trobat el producte que buscaves");
	}  
	
	public static void VisualitzarProductes(Hashtable<String, Double> productesCompra, Hashtable<String, Double> preuProductes) {
		Enumeration<String> producte = productesCompra.keys();
		Enumeration<Double> quantitat = productesCompra.elements();
		Enumeration<Double> preu = preuProductes.elements();
		while(producte.hasMoreElements()) {
			System.out.println("-----------\nProducte: " + producte.nextElement() + " \nquantitat: " + quantitat.nextElement() +"\nPreu: " + preu.nextElement());
		}
	}
	
	public static Hashtable<String, Double> afegirHash(Hashtable<String, Double> hastable, double numero, String producte){
		hastable.put(producte, numero);
		return hastable;
	}
	
	
	public static void afegirNouProducte(Scanner teclat, Hashtable<String, Double> productesCompra, Hashtable<String, Double> preuProductes) {
		System.out.println("--------------------------\nNom del producte que voleu afegir:");
		String producte = teclat.next();
		System.out.println("Preu del producte: ");
		double preu = teclat.nextDouble();
		System.out.println("Quantitat en stock: ");
		double quantitat = teclat.nextDouble();
		productesCompra = afegirHash(productesCompra, quantitat, producte);
		preuProductes = afegirHash(preuProductes, preu, producte);
		boolean comprobarRepetit = comprobarRepetit(productesCompra, producte);
		if(comprobarRepetit)
			System.out.println("El producte que esta intentant inserir ja est� a la bd");
		else
			System.out.println("S'ha afegit el nou producte");		
		
	}
	
	public static boolean comprobarRepetit(Hashtable<String, Double> productesCompra, String producte) {
		Enumeration<String> productes = productesCompra.keys();
		boolean trobat = false;
		while(productes.hasMoreElements()) {
			if(producte.equals(productes.nextElement())) {
			trobat = true;
			
			}
			
		}
		return trobat;
	}
	
	public static int opcio(Scanner teclat) {
		System.out.println("Quina acci� voleu realitzar?");
		System.out.println("1. Afegir nou producte");
		System.out.println("2. Veure tots els productes");
		System.out.println("3. Veure un producte concret");
		System.out.println("4. Sortir");
		boolean opcValida = false;int opcio = 0;
		while(!opcValida) {
			System.out.println("Escolleix una opci�:");
			opcio = teclat.nextInt();
			if(opcio==1|opcio==2|opcio==3|opcio==4)
				opcValida = true;
			
			
		}
		return opcio;	
		
	}
	public static Hashtable<String, Double> preuProductes (Hashtable<String, Double> preuProductes) {
		preuProductes.put("llet",0.80);
		preuProductes.put("shampoo",1.80);
		preuProductes.put("cereals",1.20);
		preuProductes.put("xocolata",1.05);
		preuProductes.put("melmelada",1.20);
		preuProductes.put("bossa cacahuets",1.20);
		preuProductes.put("pipes",1.10);
		preuProductes.put("fruita",0.70);
		preuProductes.put("verdura",0.80);
		preuProductes.put("pasta",0.60);
		
		return preuProductes;
	}
	
	public static Hashtable<String, Double> productesCompra (Hashtable<String, Double> productesCompra) {
		productesCompra.put("llet",3.0);
		productesCompra.put("shampoo",4.0);
		productesCompra.put("cereals",2.0);
		productesCompra.put("xocolata",1.0);
		productesCompra.put("melmelada",1.0);
		productesCompra.put("bossa cacahuets",5.0);
		productesCompra.put("pipes",4.0);
		productesCompra.put("fruita",7.0);
		productesCompra.put("verdura",8.0);
		productesCompra.put("pasta",2.0);
		
		return productesCompra;
	}
		



	
}
